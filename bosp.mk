
ifdef CONFIG_EXTERNAL_LOG4CPP

# Targets provided by this project
.PHONY: log4cpp clean_log4cpp

# Add this to the "external" target
external: log4cpp
clean_external: clean_log4cpp

MODULE_DIR_LOG4CPP=external/required/log4cpp

log4cpp: setup $(BUILD_DIR)/lib/liblog4cpp.so
$(BUILD_DIR)/lib/liblog4cpp.so:
	@echo
	@echo "==== Building Log4CPP library v1.0 ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@cd $(MODULE_DIR_LOG4CPP) && \
		autoreconf -fi
	@cd $(MODULE_DIR_LOG4CPP) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		./configure \
		--host=$(PLATFORM_TARGET) \
		--prefix=$(BUILD_DIR) --with-pic \
		--with-gnu-ld --disable-dependency-tracking \
		--enable-shared
	@cd $(MODULE_DIR_LOG4CPP) && \
		make -j$(CPUS) install
	@echo

clean_log4cpp:
	@echo
	@echo "==== Clean-up Log4CPP library v1.0 ===="
	@[ ! -f $(BUILD_DIR)/lib/liblog4cpp.so ] || \
		rm -f $(BUILD_DIR)/lib/liblog4cpp*
	@cd $(MODULE_DIR_LOG4CPP) && make clean
	@echo

else # CONFIG_EXTERNAL_LOG4CPP

log4cpp:
	$(warning $(MODULE_DIR_LOG4CPP) module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_LOG4CPP

